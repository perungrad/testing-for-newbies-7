<?php

namespace DataProvider;

use Nette\Utils\Json;
use Entity\TodoItem;
use DataConverter\TodoItemConverter;

class TodoDataProvider
{
    /** @var string */
    private $pathToFile;

    /** @var TodoItemConverter */
    private $todoItemConverter;

    public function __construct(string $pathToFile, TodoItemConverter $todoItemConverter)
    {
        $this->pathToFile        = $pathToFile;
        $this->todoItemConverter = $todoItemConverter;
    }

    public function findAll(): array
    {
        $contents = file_get_contents($this->pathToFile);

        $items = [];

        if (($contents !== false) && ($contents !== '')) {
            $items = Json::decode($contents, Json::FORCE_ARRAY);
        }

        if (!$items) {
            return [];
        }

        return array_map(function ($item) {
            return $this->todoItemConverter->convertToEntity($item);
        }, $items);
    }

    public function saveItems(array $todoItems)
    {
        $itemsArr = array_map(function (TodoItem $todoItem) {
            return $this->todoItemConverter->convertToArray($todoItem);
        }, $todoItems);

        $contents = Json::encode($itemsArr, Json::PRETTY);

        file_put_contents($this->pathToFile, $contents);
    }
}
