<?php

namespace DataConverter;

use Entity\TodoItem;

class TodoItemConverter
{
    public function convertToArray(TodoItem $todoItem): array
    {
        return [
            'id'        => $todoItem->getId(),
            'text'      => $todoItem->getText(),
            'createdAt' => $todoItem->getCreatedAt()->format('Y-m-d H:i:s'),
            'isDone'    => $todoItem->isDone(),
        ];
    }

    public function convertToEntity(array $itemData): TodoItem
    {
        $todoItem = new TodoItem();

        $todoItem->setId($itemData['id'])
            ->setText($itemData['text'])
            ->setCreatedAt(new \DateTime($itemData['createdAt']))
            ->setIsDone($itemData['isDone']);

        return $todoItem;
    }
}
