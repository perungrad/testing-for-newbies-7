<?php

namespace Entity;

class TodoItem
{
    /** @var int */
    private $id;

    /** @var string */
    private $text;

    /** @var \DateTime */
    private $createdAt;

    /** @var bool */
    private $isDone = false;

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $text
     *
     * @return self
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return self
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param bool $isDone
     *
     * @return self
     */
    public function setIsDone($isDone)
    {
        $this->isDone = $isDone;
    }

    /**
     * @return self
     */
    public function setAsDone()
    {
        $this->isDone = true;

        return $this;
    }

    /**
     * @return self
     */
    public function setAsNotDone()
    {
        $this->isDone = false;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDone()
    {
        return $this->isDone;
    }
}
